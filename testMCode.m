% Set effective support and grid parameters. 
lb = -8; ub = 8; n = 1024; 

% Compute and plot Meyer wavelet and scaling functions. 
[phi,psi,x] = meyer(lb,ub,n); 
subplot(211), plot(x,psi) 
title('Meyer wavelet') 
subplot(212), plot(x,phi) 
title('Meyer scaling function')

%this has been changed